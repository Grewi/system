<?php
namespace system\core\model;
use system\core\database\database;

trait deleteTrait
{
    public function _delete(int $id = null)
    {
        if(is_null($id)){
            $db = database::connect();
            $sql = 'DELETE FROM ' . $this->table . ' ' . $this->where;
            $db->query($sql, $this->bind);
        }else{
            $db = database::connect();
            $sql = 'DELETE FROM ' . $this->table . ' WHERE `' . $this->id . '` = ' . $id;
            $db->query($sql, [$this->id => $id]);
        }

    }
}