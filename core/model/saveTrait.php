<?php
namespace system\core\model;

trait saveTrait
{
    protected function save($data = [])
    {
        $arr = [];
        foreach($this as $a => $i){
            $arr[$a] = $i;
        }
        $arr = array_merge($arr, $data);
        return $this->update($arr);
    }
}