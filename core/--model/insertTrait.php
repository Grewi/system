<?php 
namespace system\core\model;
use system\core\database\database;

trait insertTrait
{
    public function _insert(array $data )
    {
        $db = database::connect();
        $count = count($data);
        $str = '';
        $c = 0;
        foreach($data as $key => $i){
            $c++;
            if($c == $count){
                $str .= ' `' . $key . '` = :' . $key . ' ';
            }else{
                $str .= ' `' . $key . '` = :' . $key . ', ';
            }

        }
        $data = array_merge($data,$this->bind);
        $sql = 'INSERT INTO ' . $this->table . ' SET ' . $str;
        $db->query($sql, $data);
        try{
            $a = $db->fetch('SELECT * FROM ' . $this->table . ' where ' . $this->id .' = LAST_INSERT_ID()', []);

            return $a;
        }catch(\Exception $e){
            //dd($e);
            
        }
        
    }
}