<?php
namespace system\core\model;

trait saveTrait
{
    protected function save()
    {
        $arr = [];
        foreach($this as $a => $i){
            $arr[$a] = $i;
        }
        $this->update($arr);
    }
}