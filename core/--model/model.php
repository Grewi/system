<?php

namespace system\core\model;
use system\core\model\baseModel;

class model extends baseModel
{
    use saveTrait;
    
    public static function __callStatic($method, $parameters)
    {
        $m = '_' . $method;
        if(method_exists((new static), $method)){
            return (new static)->$method(...$parameters);
        }elseif(method_exists((new static), $m)){
            return (new static)->$m(...$parameters);
        }  
    }

    public function __call($method, $param)
    {
        $m = '_' . $method;
        if(method_exists($this, $method)){
            return $this->$method(...$param);
        }elseif(method_exists($this, $m)){
            return $this->$m(...$param);
        }
    }
}
