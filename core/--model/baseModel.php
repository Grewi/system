<?php

namespace system\core\model;

use system\core\model\insertTrait;
use system\core\model\updateTrait;
use system\core\model\deleteTrait;

abstract class baseModel
{
    use insertTrait;
    use updateTrait;
    use deleteTrait;
    

    private $table = '';
    private $idNumber = 0;
    private $id = 'id';
    private $from;
    private $paginCount = 20;
    private $where = '';
    private $this_where_count = 1;
    private $bind = [];
    private $limit = '';
    private $limitDirection = 20;
    private $sort = '';
    private $sortDirection = 'DESC'; //ASC or DESC
    private $select = '*';
    private $offset = '';
    private $leftJoin = '';

    private $paginationLine = [];
    private $paginationPriv = 0;
    private $paginationNext = 0;
    private $paginationActive = 0;

    private $data = [];

    public function __construct()
    {
        if (empty($this->table)) {
            $c = explode('\\', get_called_class());
            $this->table = array_pop($c);
        }
        $this->from = $this->table;
    }

    protected function _from($from)
    {
        $this->from = $from;
        return $this;
    }

    protected function _where($p1, $p2 = null, $p3 = null)
    {
        $count = $this->this_where_count++;
        $sep = $this->where == '' ? ' WHERE' : ' AND';
        $pp1 = str_replace('.', '_', $p1) . '_' . $count;
        if (is_null($p2) && is_null($p3)) {
            $this->where .= $sep . ' `' . $this->id . '` = :' . $this->id . ' ';
            $this->bind[$this->id] = $p1;
            $this->data = array_merge([$this->id => $p1]);
            $this->idNumber = $p1;
        } elseif (is_null($p3)) {
            $this->where .= $sep . ' ' . $p1 . ' = :' . $pp1  . ' ';
            $this->bind[$pp1] = $p2;
            $this->data = array_merge([$p1 => $p2]);
            if ($p1 == $this->id) {
                $this->idNumber = $p2;
            }
        } else {
            $this->where .= $sep . ' ' . $p1 . ' ' . $p2 . ' :' . $pp1 . ' ';
            $this->bind[$pp1] = $p3;
            $this->data = array_merge([$p1 => $p3]);
            if ($p1 == $this->id) {
                $this->idNumber = $p3;
            }
        }
        return $this;
    }

    protected function _whereNull($p1)
    {
        $sep = $this->where == '' ? ' WHERE' : ' AND';
        $this->where .= $sep . ' `' . $p1 . '` IS NULL ';
        return $this;
    }

    protected function _whereNotNull($p1)
    {
        $sep = $this->where == '' ? ' WHERE' : ' AND';
        $this->where .= $sep . ' `' . $p1 . '` IS NOT NULL ';
        return $this;
    }

    protected function _whereIn($p1, $arg)
    {
        $sep = $this->where == '' ? ' WHERE' : ' AND';
        $arr = [];
        foreach ($arg as $i) {
            $count = $this->this_where_count++;
            $pp1 = str_replace('.', '_', $p1) . '_' . $count;
            $this->bind[$pp1] = $i;
            $arr[] = ':'.$pp1;
        }
        $str = implode(',', $arr);
        $this->where .= $sep . ' ' . $p1 . ' IN (' . $str . ')';
        return $this;
    }

    protected function _whereStr(string $str, array $bind = [])
    {

        $this->where .= $str;
        foreach ($bind as $key => $i) {
            $this->bind[$key] = $i;
        }
        return $this;
    }



    protected function _leftJoin($tableName, $firstTable, $secondaryTable)
    {
        $lj = ' LEFT JOIN ' . $tableName . ' ON ' . $firstTable . ' = ' . $secondaryTable . ' ';
        $this->leftJoin = $this->leftJoin . $lj;
        return $this;
    }


    protected function _pagin($limit = null, $sortById = true): model
    {
        if (isset($_GET['str']) && is_numeric($_GET['str']) && $_GET['str'] > 0) {
            $str = (int) $_GET['str'];
        } else {
            $str = 1;
        }

        if (is_null($limit)) {
            $limit = $this->limitDirection;
        }

        $this->offset = ' OFFSET ' . (($str * $limit) - $limit);
        if ($sortById) {
            $this->sort = ' ORDER BY ' . $this->id . ' ' . $this->sortDirection;
        }

        $this->limit = ' LIMIT ' . $limit;

        $count = $this->_count(); // Количество строк в базе
        $countStr = ceil($count / $limit); // Количество страниц
        if ($countStr > 1) {
            $r = [];
            for ($i = 1; $i <= $countStr; $i++) {
                $r[$i] = $i == $str ? 'active' : '';
            }
            $this->paginationLine = $r;
        }

        if ($str > 1) {
            $this->paginationPriv = $str - 1;
        }

        if ($str < $countStr) {
            $this->paginationNext = $str + 1;
        }
        $this->paginationActive = $str;

        return $this;
    }

    protected function _pagination(string $url = null)
    {
        return [
            'line' => $this->paginationLine,
            'priv' => $this->paginationPriv,
            'next' => $this->paginationNext,
            'url'  => $url,
            'actual' => $this->paginationActive,
        ];
    }

    protected function _select($select)
    {
        $this->select = $select;
        return $this;
    }

    protected function _limit($limit)
    {
        $this->limit = ' LIMIT ' . $limit . ' ';
        return $this;
    }

    protected function _sort($type, $name = null)
    {
        $name = $name ? $name : $this->id;
        if ($type == 'asc') {
            $this->sort = ' ORDER BY ' . $name . ' ASC';
        } elseif ($type == 'desc') {
            $this->sort = ' ORDER BY ' . $name . ' DESC';
        }
        return $this;
    }

    protected function _count(): string
    {
        $str = 'SELECT COUNT(*) as count FROM ' .
            $this->from . ' ' .
            $this->leftJoin . ' ' .
            $this->where;
        return db()->fetch($str, $this->bind, get_class($this))->count;
    }

    public function _summ($name): float
    {
        $str = 'SELECT SUM(`' . $name . '`) as `summ` FROM ' .
            $this->from . ' ' .
            $this->leftJoin . ' ' .
            $this->where;
        return (int)db()->fetch($str, $this->bind, get_class($this))->summ;
    }

    protected function _all()
    {
        $str = 'SELECT ' . $this->select . ' ' . ' FROM ' .
            $this->from . ' ' .
            $this->leftJoin . ' ' .
            $this->where . ' ' .
            $this->sort . ' ' .
            $this->limit . ' ' .
            $this->offset;
        // dd($str, $this->bind);
        return db()->fetchAll($str, $this->bind, get_class($this));
    }

    protected function _get()
    {
        $str = 'SELECT ' . $this->select . ' ' . ' FROM ' .
            $this->from . ' ' .
            $this->leftJoin . ' ' .
            $this->where . ' ' .
            $this->sort . ' ' .
            $this->limit . ' ' .
            $this->offset;

        return db()->fetch($str, $this->bind, get_class($this));
    }

    protected function _sql()
    {
        $str = 'SELECT ' . $this->select . ' ' . ' FROM ' .
            $this->from . ' ' .
            $this->leftJoin . ' ' .
            $this->where . ' ' .
            $this->sort . ' ' .
            $this->limit . ' ' .
            $this->offset;
        print_r($this->bind);
        dd($str);
    }

    protected function _find($id)
    {
        return db()->fetch('SELECT * FROM ' . $this->table . ' WHERE `' . $this->id . '` = :' . $this->id . ' ', [$this->id => $id], get_class($this));
    }
}
