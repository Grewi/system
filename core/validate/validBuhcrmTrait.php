<?php
namespace system\core\validate;

trait validBuhcrmTrait
{
    public function inn(): validate
    {
        $data = $this->data[$this->currentName];
        if (!empty($data) && ( mb_strlen($data) != 10 && mb_strlen($data) != 12) ) {
            // dd(123);
            $this->error[$this->currentName][] = 'ИНН может содержать 10 или 12 символов';
            $this->setControl(false);
        }
        $this->setReturn($data);
        return $this;
    } 

    public function kpp(): validate
    {
        $data = $this->data[$this->currentName];
        if (!empty($data) && mb_strlen($data) != 9 ) {
            $this->error[$this->currentName][] = 'КПП может содержать только 9 символов';
            $this->setControl(false);
        }
        $this->setReturn($data);
        return $this;
    }
    
    public function bik(): validate
    {
        $data = $this->data[$this->currentName];
        if (!empty($data) && mb_strlen($data) != 9 ) {
            $this->error[$this->currentName][] = 'БИК может содержать только 9 символов';
            $this->setControl(false);
        }
        $this->setReturn($data);
        return $this;
    } 

    public function schet(): validate
    {
        $data = $this->data[$this->currentName];
        if (!empty($data) && mb_strlen($data) != 20 ) {
            $this->error[$this->currentName][] = 'Номер счёта может содержать только 20 символов';
            $this->setControl(false);
        }
        $this->setReturn($data);
        return $this;
    }     
}