<?php 
namespace system\core\model;
use system\core\database\database;

trait updateTrait
{
    public function _update($data)
    {
        if(is_object($data)){
            $data = get_object_vars($data);
        }
        if(isset($data[$this->id])){
            $this->where($data[$this->id]);
            unset($data[$this->id]);
        }
        $count = count($data);
        $str = '';
        $c = 0;

        foreach($data as $key => $i){
            $c++;
            if($c == $count){
                $str .= ' `' . $key . '` = :' . $key . ' ';
            }else{
                $str .= ' `' . $key . '` = :' . $key . ', ';
            }
        }
        
        $data = array_merge($data, $this->bind);
        $sql = 'UPDATE ' . $this->table . ' SET ' . $str . $this->where;
        db()->query($sql, $data);
        if($this->idNumber){
            return db()->fetch('SELECT * FROM ' . $this->table . ' WHERE `' . $this->id . '` = ' . $this->idNumber . ';', []);
        }
    }
}